package main

// #cgo LDFLAGS: libc_urg.so -lm -lwinmm
// #include "urg_ctrl.h"
import "C"
import "errors"
import "fmt"

const DEVICE = "COM7"
const BAUDRATE = 115200

type Urg struct {
	curg *C.urg_t
}

func (u *Urg) Connect() error {
	ret := C.urg_connect(u.curg, C.CString(DEVICE), C.long(BAUDRATE))
	if ret < 0 {
		return errors.New("Unable to connect")
	}
	return nil
}

func main() {
	u := &Urg{new(C.urg_t)}
	
	err := u.Connect()
	if err != nil {
		fmt.Println("Connection error.")
	} else {
		fmt.Println("Connected OK.")
	}
}